#
#  Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory


def TRKNtupleAlgCfg(flags, name="TRKNtuple", **kwargs):
    acc = ComponentAccumulator()

    histsvc = CompFactory.THistSvc(name="THistSvc",
                                   Output=[ f"TRKNtuple DATAFILE='{flags.Output.HISTFileName}' OPT='RECREATE'" ])
    acc.addService(histsvc)

    if flags.PhysVal.IDPVM.runDecoration:
        from InDetPhysValMonitoring.InDetPhysValDecorationConfig import AddDecoratorCfg
        acc.merge(AddDecoratorCfg(flags))
                                
    kwargs.setdefault("IsHitInfoAvailable", False)
    kwargs.setdefault("IsMC", flags.Input.isMC)

    algo = CompFactory.TRKNtupleAlg(**kwargs)
    acc.addEventAlgo(algo)
    return acc
