#ifndef TRKANALYSIS_TRKNTUPLEALG_H
#define TRKANALYSIS_TRKNTUPLEALG_H

#include "AthenaBaseComps/AthAlgorithm.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"

#include "TTree.h"

using namespace std;

class TRKNtupleAlg: public ::AthAlgorithm { 
 public: 
  TRKNtupleAlg( const std::string& name, ISvcLocator* pSvcLocator );
  virtual ~TRKNtupleAlg(); 


  virtual StatusCode  initialize();     //once, before any input is loaded
  virtual StatusCode  execute();        //per event
  virtual StatusCode  finalize();       //once, after all events processed

 private: 

  virtual void clean();

  string m_trkParticleName;
  string m_truthParticleName;
  string m_truthVertexContainerName;
  string m_vertexContainerName;
  string m_truthEventName;
  string m_pileupEventName;

  bool m_isHitInfoAvailable;
  bool m_isMC;

  TTree* m_myTree = 0;
  ServiceHandle<ITHistSvc> m_thistSvc;

  long _EventNumber;

  int _track_n;
  vector<bool> _track_isInOut;
  vector<bool> _track_isLoose;
  vector<bool> _track_isTightPrimary;
  vector<int> _track_charge;
  vector<float> _track_pt;
  vector<float> _track_eta;
  vector<float> _track_theta;
  vector<float> _track_phi;
  vector<float> _track_phi0;
  vector<float> _track_d0;
  vector<float> _track_z0;
  vector<float> _track_z0st;
  vector<float> _track_qOverP;
  vector<float> _track_qOverPt;

  vector<float> _track_d0err;
  vector<float> _track_z0err;
  vector<float> _track_phierr;
  vector<float> _track_thetaerr;
  vector<float> _track_z0sterr;
  vector<float> _track_qOverPterr;
  vector<float> _track_prob;

  vector<int> _track_innermostPixLayerHits;
  vector<int> _track_innermostPixLayerOutliers;
  vector<int> _track_innermostPixLayerSplitHits;
  vector<int> _track_innermostPixLayerSharedHits;
  vector<int> _track_innermostExpectPixLayerHits;

  vector<int> _track_nextToInnermostPixLayerHits;
  vector<int> _track_nextToInnermostPixLayerOutliers;
  vector<int> _track_nextToInnermostPixLayerSplitHits;
  vector<int> _track_nextToInnermostPixLayerSharedHits;
  vector<int> _track_nextToInnermostExpectPixLayerHits;

  vector<int> _track_pixHits;
  vector<int> _track_pixHoles;
  vector<int> _track_pixSharedHits;
  vector<int> _track_pixOutliers;
  vector<int> _track_pixContribLayers;
  vector<int> _track_pixSplitHits;
  vector<int> _track_pixGangedHits;
  vector<int> _track_pixDead;

  vector<int> _track_SCTHits;
  vector<int> _track_SCTHoles;
  vector<int> _track_SCTDoubleHoles;
  vector<int> _track_SCTSharedHits;
  vector<int> _track_SCTOutliers;
  vector<int> _track_SCTDead;

  vector<int> _track_TRTHits;
  vector<int> _track_TRTHTHits;
  vector<int> _track_TRTOutliers;
  vector<int> _track_TRTHTOutliers;
  
  vector<int> _track_SiHits;
  vector<int> _track_SiSharedHits;

  vector<float> _track_fit_chi2;
  vector<int> _track_fit_ndf;
  vector<float> _track_fit_chi2ndf;
  vector<float> _track_fit_chi2prob; 

  vector<vector<float>> _track_cov_matrix;
  vector<float> _track_cov_matrix_det;

  vector<long> _track_patternInfo;

  vector<float> _track_eProbabilityHT;
  vector<float> _track_eProbabilityComb;

  vector<int> _track_truth_index;

  int _truth_n;
  vector<int> _truth_charge;
  vector<float> _truth_pt;
  vector<float> _truth_eta;
  vector<float> _truth_theta;
  vector<float> _truth_phi;
  vector<float> _truth_phi0;
  vector<float> _truth_mass;
  vector<float> _truth_d0;
  vector<float> _truth_z0;
  vector<float> _truth_qOverP;
  vector<float> _truth_qOverPt;
  vector<float> _truth_z0st;
  vector<float> _truth_prodR;
  vector<float> _truth_prodZ;
  vector<int> _truth_barcode;
  vector<bool> _truth_isCharged;
  vector<int> _truth_status;
  vector<int> _truth_pdgId;
  vector<bool> _truth_hasGrandParent;
  vector<bool> _truth_eleFromGamma;
  vector<bool> _truth_hasProdVtx;
  vector<bool> _truth_isFromHSProdVtx;
  vector<float> _truth_prodVtxRadius;
  vector<float> _truth_prodVtxZ;
  vector<bool> _truth_hasDecayVtx;
  vector<float> _truth_decayVtxRadius;
  vector<float> _truth_decayVtxZ;
  vector<bool> _truth_StdSel;
  vector<bool> _truth_StdSel_ITk;
  vector<bool> _truth_isFromSUSY;
  vector<int> _truth_SUSY_index;
  vector<float> _truth_SUSY_deta;
  vector<float> _truth_SUSY_dphi;
  vector<float> _truth_PVDV_deta;
  vector<float> _truth_PVDV_dphi;

  vector<int> _truth_track_index;

  vector<int> _truth_parent_index;
  vector<vector<int> > _truth_child_index;
  vector<int> _truth_n_children;
  vector<bool> _truth_isFromTau;
  vector<int> _truth_tau_parent_index;
  vector<bool> _truth_isFromB;
  vector<int> _truth_B_parent_index;

    
  int _gen_vtx_n;
  float _gen_vtx_x;
  float _gen_vtx_y;
  float _gen_vtx_z;
  float _gen_vtx_delta_z_closest;
  float _gen_vtx_sumPt2;

  vector<float> _gen_vertices_x;
  vector<float> _gen_vertices_y;
  vector<float> _gen_vertices_z;
  vector<bool> _gen_vertices_isHS;
  vector<float> _gen_vertices_sumPt2;


  int _reco_vtx_n;
  float _reco_vtx_x;
  float _reco_vtx_y;
  float _reco_vtx_z;

  vector<float> _reco_vertices_x;
  vector<float> _reco_vertices_y;
  vector<float> _reco_vertices_z;

  float _mu;

  vector<int> _pixCluster_track_index;
  vector<float> _pixCluster_reco_globX;
  vector<float> _pixCluster_reco_globY;
  vector<float> _pixCluster_reco_globZ;
  vector<float> _pixCluster_reco_globR;
  vector<float> _pixCluster_reco_globEta;
  vector<float> _pixCluster_reco_globPhi;
  vector<float> _pixCluster_reco_locX;
  vector<float> _pixCluster_reco_locY;
  vector<int> _pixCluster_bec;
  vector<int> _pixCluster_layer;
  vector<int> _pixCluster_sizePhi;
  vector<int> _pixCluster_sizeZ;
  vector<float> _pixCluster_omegax;
  vector<float> _pixCluster_omegay;
  vector<float> _pixCluster_LorentzShift;
  vector<int> _pixCluster_etaModule;
  vector<int> _pixCluster_phiModule;
  vector<float> _pixCluster_centroid_xphi;
  vector<float> _pixCluster_centroid_xeta;
  vector<uint64_t> _pixCluster_pixelID;
  vector<int>   _pixCluster_waferID;
  vector<int>   _pixCluster_hitDeco_index;

  vector<int>   _pixCluster_truth_size;
  vector<float> _pixCluster_truth_locX;
  vector<float> _pixCluster_truth_locY;
  vector<int>   _pixCluster_truth_barcode;
  vector<int>   _pixCluster_truth_reco_index;

  vector<int> _hitDeco_track_index;
  vector<int> _hitDeco_measurement_region;
  vector<int> _hitDeco_measurement_det;
  vector<int> _hitDeco_measurement_iLayer;
  vector<int> _hitDeco_measurement_type;
  vector<float> _hitDeco_residualLocX;
  vector<float> _hitDeco_residualLocY;
  vector<int> _hitDeco_phiWidth;
  vector<int> _hitDeco_etaWidth;
  vector<float> _hitDeco_measurementLocX;
  vector<float> _hitDeco_measurementLocY;
  vector<float> _hitDeco_measurementLocCovX;
  vector<float> _hitDeco_measurementLocCovY;
  vector<float> _hitDeco_trackParamLocX;
  vector<float> _hitDeco_trackParamLocY;
  vector<float> _hitDeco_angle;
  vector<float> _hitDeco_loceta;
  vector<uint64_t> _hitDeco_surfaceID;

  vector<int> _stripCluster_track_index;
  vector<float> _stripCluster_reco_globX;
  vector<float> _stripCluster_reco_globY;
  vector<float> _stripCluster_reco_globZ;
  vector<float> _stripCluster_reco_globR;
  vector<float> _stripCluster_reco_globEta;
  vector<float> _stripCluster_reco_globPhi;
  vector<float> _stripCluster_reco_locX;
  vector<float> _stripCluster_reco_locY;
  vector<int> _stripCluster_bec;
  vector<int> _stripCluster_layer;
  vector<int> _stripCluster_siWidth;
  vector<int> _stripCluster_etaModule;
  vector<int> _stripCluster_phiModule;
  vector<int> _stripCluster_side;
  vector<uint64_t> _stripCluster_stripID;
  vector<int> _stripCluster_hitDeco_index;

  vector<int> _stripCluster_truth_size;
  vector<float> _stripCluster_truth_locX;
  vector<float> _stripCluster_truth_locY;
  vector<int>   _stripCluster_truth_barcode;
  vector<int> _stripCluster_truth_reco_index;

}; 

#endif //> !TRKANALYSIS_TRKNTUPLEALG_H
