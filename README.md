**Installation instructions**

```mkdir myProject
cd myProject
setupATLAS
lsetup git
git atlas init-workdir https://gitlab.cern.ch/tstreble/athena.git
cd athena
git clone https://gitlab.cern.ch/tstreble/trkanalysis.git TRKAnalysis
cd ..
mkdir build
cd build
asetup Athena,main,latest
cmake ../athena/Projects/WorkDir
make -j
source x*/setup.sh
cd ..
mkdir run
cd run/
runTRKAnalysis.py --input AOD.pool.root #Adapt input files if needed
```
